CREATE TABLE Doctors (
   doctorID char(3) NOT NULL,
   firstName varchar (20) NOT NULL,
   lastName varchar (20) NOT NULL,
           type varchar (20) NOT NULL,
   address varchar (30) NOT NULL,
   city varchar(30) NOT NULL,
   province char(2) NOT NULL,
   postalCode char(6) NOT NULL,
   PRIMARY KEY (doctorID)
);

insert into Doctors values('111','Ted', 'Mali', 'Dentist', '11 Green Road', 'Toronto','ON','M1Y2H2');
insert into Doctors values('222','Lora', 'Brown', 'Dentist', '12 Red Road', 'Toronto','ON','M1Y2H2');
insert into Doctors values('333','Lora', 'Brown', 'Family', '13 Blue Road', 'Toronto','ON','M1Y2H2');
