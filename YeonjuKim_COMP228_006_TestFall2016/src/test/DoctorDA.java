package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DoctorDA {
	// Database access information
    private static final String URL = "jdbc:derby:clinic";
    private static final String USERNAME = "yeonju";
    private static final String PASSWORD = "yeonju";
    
    private Connection connection;
    
    private PreparedStatement selectDoctorByDocType;
    
    public DoctorDA() throws ClinicException {
    	try {
			connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
			
			selectDoctorByDocType = connection.prepareStatement("SELECT * FROM Doctors WHERE type = ?");
			
		} catch (SQLException e) {
			throw new ClinicException( e.getMessage() );
		}
    }
    
    // Get doctors data from DB.
    public ArrayList<Doctor> getDoctorsByType(String type) throws ClinicException
    {
    	ArrayList<Doctor> list = new ArrayList<Doctor>();
        ResultSet dataFromDB = null;
        
        try {
        	selectDoctorByDocType.setString(1, type);
            
            // Execute SQL and get the return
            dataFromDB = selectDoctorByDocType.executeQuery();
            
            // Process data from database           
            while( dataFromDB.next() ) {      
                Doctor data = new Doctor();
                
                data.setDoctorID( dataFromDB.getInt("doctorID") );
                data.setFirstName( dataFromDB.getString("firstName") );
                data.setLastName( dataFromDB.getString("lastName") );
                data.setType( dataFromDB.getString("type") );
                data.setAddress( dataFromDB.getString("address") );
                data.setCity( dataFromDB.getString("city") );
                data.setProvince( dataFromDB.getString("province") );
                data.setPostalCode( dataFromDB.getString("postalCode") );
                
                list.add(data);
            }
        } catch (SQLException sqlException){
            close();
            throw new ClinicException( sqlException.getMessage() );
        }
        
    	return list;
    }
    
    public void close() throws ClinicException {
        try {
            connection.close();
        } catch (SQLException sqlException){
            throw new ClinicException( sqlException.getMessage() );
        }
    }
    
}
