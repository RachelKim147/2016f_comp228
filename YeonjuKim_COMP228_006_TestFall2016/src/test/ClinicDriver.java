package test;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class ClinicDriver {
	
	private static DoctorDA doctorDA;

	public static void main(String[] args) throws ClinicException {
		try {
			// Create doctor data access class.
			doctorDA = new DoctorDA();
			
			// Get input from input dialog.
			String typeOfDoc = displaySearchingUI();
			
			// Get specific type of doctors' information.
			ArrayList<Doctor> list = doctorDA.getDoctorsByType(typeOfDoc);
			String docList = "";
			
			// Create string for displaying.
			// If there is no doctors,
			if( list.isEmpty()) {
				docList = String.format("There is no type of doctors.%nSelected type:%s", typeOfDoc);
			}
			// If there are at least one more doctors,
			else {
				for( Doctor doc : list ) {
					docList += "[Doctor infomation]\n";
					docList += doc.getDisplayInfo();
					docList += "------------------------------------------\n";
				}
			}	
			
			// Display the information.
			JOptionPane.showMessageDialog(null, 
					docList,
					"Clinic", 
					JOptionPane.PLAIN_MESSAGE);
			
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, 
						e.getMessage(),
						"Clinic", 
						JOptionPane.ERROR_MESSAGE);
		}	
	}
	
	// Display input dialog.
	private static String displaySearchingUI()
	{
		String returnValue = JOptionPane.showInputDialog( "Please enter the type of doctors you want to see:" );
		
		return returnValue;
	}
}
