package test;

public class ClinicException extends Exception {
	
	// Constructor
    public ClinicException(String msg) {
        super(msg);
    }
    
}
