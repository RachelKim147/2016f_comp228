package test;

public class Doctor {
	private int doctorID;
	private String firstName;
	private String lastName;
	private String type;
	private String address;
	private String city;
	private String province;
	private String postalCode;
	
	public Doctor() {
		
	}
	
	// Getters and setters
	protected void setDoctorID(int doctorID) {
		this.doctorID = doctorID;
	}
	protected void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	protected void setLastName(String lastName) {
		this.lastName = lastName;
	}
	protected void setType(String type) {
		this.type = type;
	}
	protected void setAddress(String address) {
		this.address = address;
	}
	protected void setCity(String city) {
		this.city = city;
	}
	protected void setProvince(String province) {
		this.province = province;
	}
	protected void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	public String getDisplayInfo()
	{
		String resultString = String.format("ID : %d %nName: %s %s %nType: %s %nAddress: %s %nCity: %s %nProvince: %s %nPostal code: %s %n", 
				doctorID, firstName, lastName, type, address, city, province, postalCode);
		
		return resultString;
	}
}
