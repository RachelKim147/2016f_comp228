package exercise1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Driver {

	public static void main(String[] args) {
		demo1();
		demo2();
	}
	
	public static void demo1()
	{
		Account myAccount = new Account(8000);

		Transaction withdrawTask = new Transaction("withdraw", myAccount);
		Transaction depositTask = new Transaction("deposit", myAccount);

		// execute the tasks with an ExecutorService
		ExecutorService executorService = Executors.newCachedThreadPool();
		executorService.execute(withdrawTask);
		executorService.execute(depositTask);

		executorService.shutdown();
	}
	
	public static void demo2()
	{
		Account myAccount = new Account(8000);

		Transaction withdrawTask = new Transaction("withdraw", myAccount);
		Transaction depositTask = new Transaction("deposit", myAccount);
		
		// Create an ArrayList that can hold objects of type Transaction.
		ArrayList<Transaction> transactions = new ArrayList<Transaction>();
		
		// Add all three transaction to the ArrayList.
		// one that you created in this step
		transactions.add(new Transaction("deposit", myAccount));
		// the other two from the step above
		transactions.add(withdrawTask);
		transactions.add(depositTask);
		
		// Using loop structure pass tasks from the ArrayList to method execute of ExecutorService to execute the threads.
		ExecutorService executorService = Executors.newCachedThreadPool();
		// Create iterator for transaction.
		Iterator<Transaction> iterator = transactions.iterator();
        while (iterator.hasNext()) {
        	executorService.execute(iterator.next());
        }
        executorService.shutdown();
	}

}
