package exercise3;

public class PersonalMortgage extends Mortgage{
	
	private double currentPrimeRate;
	private final String mortgageName;
	
	public PersonalMortgage() {
		super();
		currentPrimeRate = 2.0;
		mortgageName = "Persnal mortgage";
	}

	public String getMortgageInfo() {
		String result = String.format("[%s]\nMortgage number: %d\nCustomer name: %s\nAmount of mortgage: $%.2f\nInterest rate: %.2f%%\nInterest prime rate: %.2f%%\nTerm: %d year(s)\nPayment: $%.2f", 
										mortgageName, number, customerName, amountOfMortgage, interestRate, currentPrimeRate, term, calculatePayment());
		return result;
	}
	
	private double calculatePayment() {
		double payment;
		
		double rate_months = currentPrimeRate + interestRate;
		rate_months = rate_months / 100 / 12.0;
		
		double term_months = term * 12; 
		
		payment = (amountOfMortgage * rate_months) / (1 - Math.pow(1 + rate_months, -term_months));
		
		return payment;
	}

}
