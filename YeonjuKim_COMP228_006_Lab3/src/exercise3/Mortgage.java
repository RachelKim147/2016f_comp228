package exercise3;

public abstract class Mortgage implements MortgageConstants {
	
	// Global variables
	protected int number;
	protected String customerName;
	protected double amountOfMortgage;
	protected double interestRate;
	protected int term;
	
	// Abstracts
	public abstract String getMortgageInfo();
	
	// Constructor
	public Mortgage() {
		number 				= 0;
		customerName 		= "";
		amountOfMortgage 	= 0;
		interestRate 		= 0;
		term 				= MortgageConstants.SHORT_TERM;
	}

	protected void setNumber(int number) {
		this.number = number;
	}

	protected void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	protected void setAmountOfMortgage(double amountOfMortgage) {
		this.amountOfMortgage = amountOfMortgage;
	}

	protected void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	protected void setTerm(int term) {
		this.term = term;
	}
	
	
	
}
