package exercise3;

import java.util.ArrayList;

import javax.swing.JOptionPane;

/**
 * 
 * @author Yeonju Kim
 * Driver class
 *
 */
public class ProcessMortgage {
	
	private static ArrayList<Mortgage> mortgages;
	
	// Menu objects
	private static Object[] mortgageTypes;
	private static Object[] termTypes;
	
	// Constant variables
	private final static int MAX_MORTGAGE_ACCOUNT_COUNT = 3;
	
	private final static int INTEREST_RATE_UNDEFINED = 0;
	
	private final static int MORTGAGE_TYPE_PERSONAL = 0;	
	private final static int MORTGAGE_TYPE_BUSINESS = 1;
	private final static int MORTGAGE_TYPE_MAX = 2;
	
	public final static int TERM_TYPE_SHORT = 0;
	public final static int TERM_TYPE_MEDIUM = 1;
	public final static int TERM_TYPE_LONG = 2;
	public final static int TERM_TYPE_MAX = 3;
	
	// Messages for menus
	private final static String WELCOME_MESSAGE = "Welcome to %s!!";
	
	private final static String INTEREST_RATE_QUESTION_MESSAGE = "Please enter the current interest rate(%%).";
	private final static String INTEREST_RATE_MESSAGE_EXCEPTION = "Invalid input!!\n(%s)\nPlease enter only number!!";
	
	private final static String MORTGAGE_TYPE_QUESTION_MESSAGE = "Please select which mortgage type you want.";
	
	private final static String AMOUNT_OF_MORTGAGE_QUESTION_MESSAGE = "Please enter the amount of mortgage(dollar).";
	private final static String AMOUNT_OF_MORTGAGE_MESSAGE_ERROR = "Your input: $%.2f\nAmount of mortgage can not be less than $0.00 or more than $%.2f";
	private final static String AMOUNT_OF_MORTGAGE_MESSAGE_EXCEPTION = "Invalid input!!\n(%s)\nPlease enter only number!!";
	
	private final static String TERM_QUESTION_MESSAGE = "Please select wanted term.";
	
	private final static String CLOSE_MESSAGE = "You clicked cancel button.\nClose the program....";
	private final static String CLOSE_REASON_CLICK_CANCEL = "You clicked cancel button.";
	private final static String CLOSE_REASON_INVALID_INSURANCE_TYPE = "Invaild insurance type[%d].";
	//-- End messages 	
	
	public static void main(String[] args) {
		init();
		
		// Current interest rate
		double wantedInterestRate = displayDialogForInterestRate();
		
		int count = 0;
		while(count < MAX_MORTGAGE_ACCOUNT_COUNT) {
			Mortgage newMortgage = null;
			
			// Mortgage type
			int wantedMortgageType = displayDialogForMortgageType();
			if( wantedMortgageType == MORTGAGE_TYPE_PERSONAL )
				newMortgage = new PersonalMortgage();
			else if ( wantedMortgageType == MORTGAGE_TYPE_BUSINESS ) 
				newMortgage = new BusinessMortgage();
			else // [TODO] Is this possible case?
				closeProgram( String.format(CLOSE_REASON_INVALID_INSURANCE_TYPE, wantedMortgageType) );
			
			// Number
			newMortgage.setNumber( count+1 );
			
			// Current interest rate
			newMortgage.setInterestRate( wantedInterestRate );

			// Amount of mortgage		
			double wantedAmountOfMortgage = displayDialogForAmountOfMortgage();
			newMortgage.setAmountOfMortgage( wantedAmountOfMortgage );
			
			// Term
			int wantedTermType = displayDialogForTerm();
			int currentTerm = 0;
			if( wantedTermType == TERM_TYPE_SHORT )
				currentTerm = MortgageConstants.SHORT_TERM;
			else if( wantedTermType == TERM_TYPE_MEDIUM )
				currentTerm = MortgageConstants.MEDIUM_TERM;
			else if( wantedTermType == TERM_TYPE_LONG )
				currentTerm =  MortgageConstants.LONG_TERM;
			else
				closeProgram( String.format(CLOSE_REASON_INVALID_INSURANCE_TYPE, wantedTermType) );		
			newMortgage.setTerm(currentTerm);
			
			// Customer name
			String customerName = displayDialogForCustomerName();
			newMortgage.setCustomerName(customerName);
			
			mortgages.add(newMortgage);
			count++;
			
			// Display the result and if user click the cancel button, close the program.
			if( JOptionPane.CLOSED_OPTION == displayCreatedMortgageInfomation(newMortgage.getMortgageInfo()) ) {
				closeProgram( String.format(CLOSE_MESSAGE) );		
			}
		}
		
		// After looping, display all information.
		String result = "[All created mortgages' information ]";
		for(Mortgage item : mortgages)
		{
			result = result + "\n\n" + item.getMortgageInfo();
		}
		
		displayCreatedMortgageInfomation(result);
			
	}
	
	// Initialize global variables
	private static void init() {
		mortgages = new ArrayList<Mortgage>();
		
		mortgageTypes = new Object[MORTGAGE_TYPE_MAX];
		mortgageTypes[MORTGAGE_TYPE_PERSONAL] = "Personal";
		mortgageTypes[MORTGAGE_TYPE_BUSINESS] = "Business";
		
		termTypes = new Object[TERM_TYPE_MAX];
		termTypes[TERM_TYPE_SHORT] = String.format("Short term(%d year(s))", MortgageConstants.SHORT_TERM);
		termTypes[TERM_TYPE_MEDIUM] = String.format("Medium term(%d year(s))", MortgageConstants.MEDIUM_TERM);
		termTypes[TERM_TYPE_LONG] = String.format("Long term(%d year(s))", MortgageConstants.LONG_TERM);
	}
	
	// Display input dialog for interest rate
	private static double displayDialogForInterestRate() {
		String dialogMessage = String.format(WELCOME_MESSAGE + "\n" + INTEREST_RATE_QUESTION_MESSAGE, 
											 MortgageConstants.BANK_NAME);
		String exceptionMessage;
		
		double interestDouble = INTEREST_RATE_UNDEFINED;	
		while( interestDouble <= INTEREST_RATE_UNDEFINED ) { 
			
			String interestString = JOptionPane.showInputDialog(null,
																dialogMessage,
																MortgageConstants.BANK_NAME,
																JOptionPane.WARNING_MESSAGE);
		
		
			try {
				interestDouble = Double.parseDouble(interestString.trim());
			} 
			catch (Exception e) {
				exceptionMessage = e.getMessage();
				if( exceptionMessage != null ) {
					JOptionPane.showMessageDialog(null, 
											  	  String.format(INTEREST_RATE_MESSAGE_EXCEPTION, exceptionMessage) ,
											  	  MortgageConstants.BANK_NAME, 
										  	      JOptionPane.WARNING_MESSAGE);
				} else { // Canceled
					closeProgram( CLOSE_REASON_CLICK_CANCEL );
				}
			}
		}
		
		return interestDouble;
	}
	
	// Display option dialog for mortgage type.
	private static int displayDialogForMortgageType() {	
		int selectedType = JOptionPane.showOptionDialog( null,
														 MORTGAGE_TYPE_QUESTION_MESSAGE,
														 MortgageConstants.BANK_NAME,
														 JOptionPane.OK_CANCEL_OPTION,
														 JOptionPane.QUESTION_MESSAGE,
														 null,
														 mortgageTypes,
														 mortgageTypes[MORTGAGE_TYPE_PERSONAL] );
		
		if( selectedType == JOptionPane.CLOSED_OPTION )  // Canceled
			closeProgram( CLOSE_REASON_CLICK_CANCEL );
														 									
		return selectedType;
	}
	
	// Display input dialog for amount of mortgage.
	private static double displayDialogForAmountOfMortgage() {
		String exceptionMessage;
		
		double mountOfMortgage = INTEREST_RATE_UNDEFINED;	
		String interestString = "";
		while( mountOfMortgage <= 0  || mountOfMortgage > MortgageConstants.MAX_MORTGAGE_AMOUNT ) { 
			
			interestString = JOptionPane.showInputDialog(null,
														 AMOUNT_OF_MORTGAGE_QUESTION_MESSAGE,
														 MortgageConstants.BANK_NAME,
														 JOptionPane.WARNING_MESSAGE);
		
		
			try {
				mountOfMortgage = Double.parseDouble(interestString.trim());
				if( mountOfMortgage <= 0  || mountOfMortgage > MortgageConstants.MAX_MORTGAGE_AMOUNT ) {
					JOptionPane.showMessageDialog(null, 
						  	  String.format(AMOUNT_OF_MORTGAGE_MESSAGE_ERROR, mountOfMortgage, MortgageConstants.MAX_MORTGAGE_AMOUNT), 
						  	  MortgageConstants.BANK_NAME, 
					  	      JOptionPane.WARNING_MESSAGE);
				}
			} 
			catch (Exception e) {
				exceptionMessage = e.getMessage();
				if( exceptionMessage != null ) {
					JOptionPane.showMessageDialog(null, 
											  	  String.format(AMOUNT_OF_MORTGAGE_MESSAGE_EXCEPTION, exceptionMessage),
											  	  MortgageConstants.BANK_NAME, 
										  	      JOptionPane.WARNING_MESSAGE);
				} else { // Canceled
					closeProgram( CLOSE_REASON_CLICK_CANCEL );
				}
			}
		}
		
		return mountOfMortgage;
	}
	
	// Display dialog for term
	private static int displayDialogForTerm() {	
		int selectedType = JOptionPane.showOptionDialog( null,
														 TERM_QUESTION_MESSAGE,
														 MortgageConstants.BANK_NAME,
														 JOptionPane.OK_CANCEL_OPTION,
														 JOptionPane.QUESTION_MESSAGE,
														 null,
														 termTypes,
														 termTypes[TERM_TYPE_SHORT] );
		
		if( selectedType == JOptionPane.CLOSED_OPTION )  // Canceled
			closeProgram( CLOSE_REASON_CLICK_CANCEL );
		
		return selectedType;
	}
	
	// Display dialog for name
	private static String displayDialogForCustomerName() {
		String customerName = "";
		while(customerName.isEmpty()) {
			customerName = JOptionPane.showInputDialog(null,
													   "Please enter your name.",
													   MortgageConstants.BANK_NAME,
													   JOptionPane.QUESTION_MESSAGE);
			
			if( customerName == null )
				closeProgram( CLOSE_REASON_CLICK_CANCEL );
			else if( customerName.isEmpty() )
				JOptionPane.showMessageDialog(null, 
					  	  "Name is empty.",
					  	  MortgageConstants.BANK_NAME, 
				  	      JOptionPane.WARNING_MESSAGE);
			
		}
		
		return customerName;
	} 
	
	// Display created mortgage information
	private static int displayCreatedMortgageInfomation(String message) {
		Object options[] = {"OK"};
		return JOptionPane.showOptionDialog(null, 
									 		message,
									 		MortgageConstants.BANK_NAME,
									 		JOptionPane.YES_NO_OPTION, 
									 		JOptionPane.INFORMATION_MESSAGE, null,
									 		options, options[0]);
	}
	
	
	// Closing dialog
	private static void closeProgram(String reason) {
		JOptionPane.showMessageDialog(null, 
				  					  String.format(CLOSE_MESSAGE, reason), // The reason that why the program is closed.
				  					  MortgageConstants.BANK_NAME, 
				  					  JOptionPane.INFORMATION_MESSAGE);
		System.exit(0);
	}
}
