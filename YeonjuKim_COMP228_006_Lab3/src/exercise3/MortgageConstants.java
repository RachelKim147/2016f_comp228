package exercise3;

public interface MortgageConstants {
	
	public final static int SHORT_TERM = 1;
	public final static int MEDIUM_TERM = 3;
	public final static int LONG_TERM = 5;
	
	public final static String BANK_NAME = "City Toronto Bank";
	
	public final static double MAX_MORTGAGE_AMOUNT = 300000.0;

}
