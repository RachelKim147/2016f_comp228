package exercise2;

public class PartTimeStudent extends Student {

	private int creditTime;
	
	public PartTimeStudent(String name, boolean bFullTime, int creditTime) {
		super(name, bFullTime);
		this.creditTime = creditTime;
	}
	
	protected double determinTheTuition() {
		
		return creditTime * 100.0;
	}
	
	public int getCreditTime() {
		return creditTime;
	}

}
