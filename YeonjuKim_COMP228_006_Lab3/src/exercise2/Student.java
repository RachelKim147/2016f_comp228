package exercise2;

public abstract class Student {
	
	// Global variables
	private String name;
	private boolean bFullTime;
	
	public Student(String name, boolean bFullTime) {
		this.name = name;
		this.bFullTime = bFullTime;
	}
	
	protected abstract double determinTheTuition();
	
	public String getName() {
		return name;
	}
	
	public String getStudyType() {
		if(bFullTime) {
			return "Full time";
		}
		
		return "Part time";
	}
}
