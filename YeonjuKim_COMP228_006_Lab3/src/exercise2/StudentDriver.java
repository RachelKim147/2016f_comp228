package exercise2;

import java.util.Scanner;

public class StudentDriver {
	
	public enum EMenuType {	
		MENU_NONE(0), //0
		MENU_CREATE(1), // 1
		MENU_CLOSE(2); //2
		
		private int value;
		
		private EMenuType() {
			
		}
		
		private EMenuType(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
		
		public static EMenuType getByNum(int num) {
			if( num == MENU_CLOSE.getValue() )
				return MENU_CLOSE;
			else if( num == MENU_CREATE.getValue() )
				return MENU_CREATE;
			else if( num == MENU_NONE.getValue() ) 
				return MENU_NONE;
			else
				return null;
		}
	}
	
	public enum EStudyTypeMenuType {
		MENU_STUDY_NONE(0),
		MENU_STUDY_FULL_TIME(1),
		MENU_STUDY_PART_TIME(2);
		
		private int value;
		private EStudyTypeMenuType() {
			
		}
		private EStudyTypeMenuType(int value) {
			this.value = value;
		}
		public int getValue() {
			return value;
		}
		public static EStudyTypeMenuType getByNum(int num) {
			if( num == MENU_STUDY_FULL_TIME.getValue() )
				return MENU_STUDY_FULL_TIME;
			else if( num == MENU_STUDY_PART_TIME.getValue() )
				return MENU_STUDY_PART_TIME;
			else if( num == MENU_STUDY_NONE.getValue() )
				return MENU_STUDY_NONE;
			else
				return null;
		}
	}
	
	// Messages for display the result
	private final static String resultMessage = "[New student infomation]\nName:%s\nType:%s\nExpected tuitions: $%.2f\n";
	private final static String resultMessageExtra = "Credit hour: %d hour(s)\n";
	
	// Menu messages
	private final static String topMenuMessage = "Please select the menu(1-Register new student, else-Close the program).:";
	private final static String studyTypeMenuMessage = "Please select the student type(1-Full time, 2-Part time).:";
	private final static String nameMenuMessage = "Please input your name.: ";
	private final static String creditHoursMessage = "Please input credit hours you want at least one hour.:";
	private final static String closeMessage = "Close the program...Thank you for using our program!";
	
	private final static String menuErrorMessage = "[System] Error! Please put correct menu number";
	private final static String creditHourErrorMessage= "[System] Error! Please enter correct credit hours.";
	
	// Methods
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		int menuNum = -1;
		EMenuType menuType = EMenuType.MENU_NONE;
		
		while(menuType != EMenuType.MENU_CLOSE) {
					
			menuNum = askWantedMenu( input, topMenuMessage ); 
			
			if( menuNum >= 0 ) {
				switch ( EMenuType.getByNum(menuNum) ) {
				case MENU_CREATE:
					Student newStudent = createStudent( input );		
					displayResult( newStudent );
					break;
				default:
					menuType = EMenuType.MENU_CLOSE;
					System.out.println(closeMessage);
					break;
				}
			}
			else {
				System.out.println( menuErrorMessage );
			}
		}
		
		input.close();
	}
	
	// display menu message and input number from console
	public static int askWantedMenu(Scanner input, String menuString) {
		
		String inputString;
		int inputInt;
		
		System.out.println(menuString);
		inputString = input.nextLine();
		
		try {
			inputInt = Integer.parseInt(inputString);
		} 
		catch (NumberFormatException | NullPointerException e) {
			inputInt = -1;
		}
		
		return inputInt;
	}
	
	// create new student information
	public static Student createStudent(Scanner input) {
		
		Student newData = null;
		int menuNum = -1;
		EStudyTypeMenuType studyType = EStudyTypeMenuType.MENU_STUDY_NONE;
		
		// Student study type
		while(studyType != EStudyTypeMenuType.MENU_STUDY_FULL_TIME && studyType != EStudyTypeMenuType.MENU_STUDY_PART_TIME ) {			
			menuNum = askWantedMenu( input, studyTypeMenuMessage );
			studyType = EStudyTypeMenuType.getByNum( menuNum );
			if( studyType != EStudyTypeMenuType.MENU_STUDY_FULL_TIME && studyType != EStudyTypeMenuType.MENU_STUDY_PART_TIME )
				System.out.println( menuErrorMessage );
		}
		
		// Student name
		System.out.println( nameMenuMessage );
		String name = input.nextLine();
		
		// Credit hour (only part time student)
		int creditHours = 0;		
		if( studyType == EStudyTypeMenuType.MENU_STUDY_PART_TIME ) {
			while( creditHours <= 0 ) {
				creditHours = askWantedMenu( input, creditHoursMessage );
								
				if( creditHours <= 0 ) { 
					System.out.println( creditHourErrorMessage );
				} else {
					newData = new PartTimeStudent( name, false, creditHours );
					break;
				}

			}
		} else {
			newData = new FullTimeStudent( name, true );
		}
		
		return newData;
	}
	
	// display the new student information
	public static void displayResult(Student newStudent) {
		
		String message = String.format(resultMessage, newStudent.getName(), newStudent.getStudyType(), newStudent.determinTheTuition());
		
		if( newStudent instanceof PartTimeStudent ) {
			
			message += String.format(resultMessageExtra, ((PartTimeStudent)newStudent).getCreditTime() );
		}
		
		System.out.println(message);
	}

}
