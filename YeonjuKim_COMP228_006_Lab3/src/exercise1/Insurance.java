package exercise1;

/**
 * 
 * @author Yeonju Kim
 * Parent class.
 *
 */
public abstract class Insurance {
	
	// Global variables
	protected String insuranceType;
	protected double monthlyCost;
	
	protected final String displayFormat = "Insurance type: %s\nMonthly Cost: $%.2f\n";
	
	// Abstract methods 
	public abstract void setInsuranceCost(double cost);
	public abstract String displayInfo();
	
	// Getter methods
	protected String getInsuranceType() {
		return insuranceType;
	}
	protected double getMonthlyCost() {
		return monthlyCost;
	}
	
	
		
}
