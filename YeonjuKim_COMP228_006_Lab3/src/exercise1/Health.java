package exercise1;

public class Health extends Insurance{
	
	public Health() {
		super();	
		insuranceType = "Health";
	}
	
	public void setInsuranceCost(double cost) {		
		monthlyCost = cost;		
	}
	

	public String displayInfo() {		
		String info = String.format(displayFormat, insuranceType, monthlyCost);	
		return info;		
	}

}
