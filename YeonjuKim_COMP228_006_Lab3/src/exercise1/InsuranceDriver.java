package exercise1;

import java.util.ArrayList;

import javax.swing.JOptionPane;

/**
 * 
 * @author Yeonju Kim 
 * Driver class
 *
 */
public class InsuranceDriver {

	// Global variables
	private static ArrayList<Insurance> insurances;
	private static Object[] insuranceTypes;
	
	// Constant variables
	public final static int INSURANCE_TYPE_UNDEFINED = -1;
	public final static int INSURANCE_TYPE_HEALTH = 0;
	public final static int INSURANCE_TYPE_LIFE = 1;
	public final static int INSURANCE_TYPE_CANCELATION = 2;
	public final static int INSURANCE_TYPE_MAX = 3;
	
	public final static double COST_UNDEFINED = 0;
	
	// Main method
	public static void main(String[] args) {	
		init();
		displayIntroDialog();
	}
	
	// Initialize variables
	private static void init() {
		insurances = new ArrayList<Insurance>();
		
		insuranceTypes = new Object[INSURANCE_TYPE_MAX];
		insuranceTypes[INSURANCE_TYPE_HEALTH] = "Health";
		insuranceTypes[INSURANCE_TYPE_LIFE] = "Life";
		insuranceTypes[INSURANCE_TYPE_CANCELATION] = "Cancel";
		
	}
	
	// The beginning method
	private static void displayIntroDialog() {
		
		Object insuranceTypes[] = {"Contract new insurance.", "Display all infomation of insurances", "Close the program"};
		
		while(true){			
			// Show the main menu and get number what customer input.
			Object selectedValue = JOptionPane.showInputDialog( null, 
					"Select the menu what you want.",
					"Intro", 
					JOptionPane.QUESTION_MESSAGE, null, 
					insuranceTypes, insuranceTypes[0] );
			
			// Create new insurance
			if( selectedValue == insuranceTypes[0] ) {
				displayNewContractDialog();
			}
			// Display all information of insurances
			else if( selectedValue == insuranceTypes[1] ) {
				displayAllInsuranceDialog();
			}
			// Close the program
			else {
				break;
			}
		}
		
	}
	
	
	// Create new insurance
	private static void displayNewContractDialog() {
		Insurance newItem = null;
	
		// Select insurance type
		int insuranceType = displayInsuranceDialog();
		
		
		if( insuranceType != INSURANCE_TYPE_CANCELATION ) {
			// Set cost for insurance
			double cost = displayCostDialog();	
			
			// Make insurance instance
			newItem = createInsurance(insuranceType, cost);
		}
		
		// Display new insurance information if it's created well.
		if( newItem != null ) {
			displayInsuranceInfomationDialog(newItem);
		} else {
			displayCanceledCreatingInsurance();
		}
	}
	
	// Display insurance type menu
	private static int displayInsuranceDialog() {	
		Object selectedValue;
		int selectedInsurance = INSURANCE_TYPE_UNDEFINED;
		
		selectedValue = JOptionPane.showInputDialog( null, 
														"Please select insurance type.",
														"Insurance Type", 
														JOptionPane.QUESTION_MESSAGE, null, 
														insuranceTypes, insuranceTypes[INSURANCE_TYPE_HEALTH] );
		
		for( int i = 0; i < insuranceTypes.length; i++ ) {
			if( selectedValue == insuranceTypes[i] ) {
				selectedInsurance = i;
				break;
			}
		}
	
		return selectedInsurance;
	}
	
	// Display insurance cost 
	private static double displayCostDialog() {		
		String returnValue;
		double cost = COST_UNDEFINED;
		
		returnValue = JOptionPane.showInputDialog( "Input a amout of insurance cost." );
			
		try{			
			cost = Double.parseDouble(returnValue.trim());				  
		} catch (NumberFormatException | NullPointerException e) {				  
			cost = COST_UNDEFINED;
		}						 		
		
		return cost;
	}
	
	// Create insurance instance
	private static Insurance createInsurance(int type, double cost) {
	
		Insurance newItem;
		
		switch(type) {
		case INSURANCE_TYPE_HEALTH : // create health insurance
			newItem = new Health();
			break;
		case INSURANCE_TYPE_LIFE : // create life insurance
			newItem = new Life();
			break;
		default:
			return null;
		}
		
		if( cost <= COST_UNDEFINED )
			return null;
		newItem.setInsuranceCost(cost);
		
		insurances.add(newItem);
		
		return newItem;
	}
	
	// display new insurance information
	private static void displayInsuranceInfomationDialog(Insurance data) {
		
		JOptionPane.showMessageDialog(null, "[The information of new insurance]\n\n" + data.displayInfo());
		
	}
	
	// display when creating is canceled
	private static void displayCanceledCreatingInsurance() {
		JOptionPane.showMessageDialog(null, "[Canceled creating insurance]\n\n Creating insurance is canceled");
	}
	
	// display all insurance informations
	private static void displayAllInsuranceDialog() {
		
		String result = "[All information of insurances]\n";
		
		for(Insurance item : insurances)
			result = String.format("%s\n%s\n", result, item.displayInfo());
		
		JOptionPane.showMessageDialog(null, result);
	}
	
	

}
