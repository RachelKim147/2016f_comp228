package exercise1;

public class Life extends Insurance{
	
	public Life() {
		super();
		
		insuranceType = "Life";
	}

	public void setInsuranceCost(double cost) {
		
		monthlyCost = cost;
		
	}

	public String displayInfo() {
		
		String info = String.format(displayFormat, insuranceType, monthlyCost);	
		return info;
		
	}

}
