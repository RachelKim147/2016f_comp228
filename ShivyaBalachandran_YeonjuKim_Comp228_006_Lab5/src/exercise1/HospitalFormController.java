/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise1;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Shivya
 */
public class HospitalFormController implements Initializable {

    @FXML
    private Button createSubmit;
    @FXML
    private TextField createName;
    @FXML
    private TextField createType;
    @FXML
    private TextField updatePatientId;
    @FXML
    private TextField updateName;
    @FXML
    private TextField updateDOB;
    @FXML
    private Button updateSearch;
    @FXML
    private Button updateSave;
    @FXML
    private TextField displayPatientID;
    @FXML
    private Button displaySearch;
    @FXML
    private TableView<DisplayInfo> displayTable;
    @FXML
    private TableColumn<DisplayInfo, String> subjectTableColumn;
    @FXML
    private TableColumn<DisplayInfo, String> descTableColumn;
    
    
    private HospitalDA hospitalDA;
    private ObservableList<DisplayInfo> data;
            
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            hospitalDA = new HospitalDA();
            
            subjectTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
            descTableColumn.setCellValueFactory(new PropertyValueFactory<>("data"));
            
        } catch (HospitalException ex) {
            Alert alert = new Alert(AlertType.INFORMATION);
            alert.setTitle("Error");
            alert.setHeaderText("Connection Error!");
            alert.setContentText(ex.getMessage());

            alert.showAndWait();
        }
    }    

    @FXML
    private void onCreateSubmit(ActionEvent event) {
        try {
            int newRow = hospitalDA.addDoctor( createName.getText(), createType.getText() );
            
            if( newRow > 0 )
            {
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle("Add doctor");
                alert.setHeaderText("Success to add doctor!");
                alert.setContentText("New row count:" + newRow);

                alert.showAndWait();
            }   
        } catch (HospitalException ex) {
            Logger.getLogger(HospitalFormController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void onUpdateSearch(ActionEvent event) {
        int searchingID = Integer.parseInt( updatePatientId.getText() );
        Patient result = null;
        try {
            result = hospitalDA.getPatientByPatientID(searchingID);
        } catch (HospitalException ex) {
            Logger.getLogger(HospitalFormController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if( result != null ) {
            updateName.setText( result.getName() );
            updateDOB.setText( result.getDateOfBirth() );
        }
    }

    @FXML
    private void onUpdateSave(ActionEvent event) {
        int searchingID = Integer.parseInt( updatePatientId.getText() );
        try {
            hospitalDA.updatePatientInformationByPatientID(searchingID, updateName.getText());
        } catch (HospitalException ex) {
            Logger.getLogger(HospitalFormController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void onDisplaySearch(ActionEvent event) {
        int searchingID = Integer.parseInt( displayPatientID.getText() );
        try {
            data = FXCollections.observableList( hospitalDA.getAllInformationByPatientID(searchingID));
            displayTable.setItems( data );
        } catch (HospitalException ex) {
            Logger.getLogger(HospitalFormController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
