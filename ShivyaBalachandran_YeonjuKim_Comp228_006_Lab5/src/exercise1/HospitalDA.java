/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise1;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Shivya
 */
public class HospitalDA {
    
    // Database access information
    private static final String URL = "jdbc:derby:hospital";
    private static final String USERNAME = "java";
    private static final String PASSWORD = "java";
    
    private Connection connection;
    
    private PreparedStatement insertNewDoctor;
    private PreparedStatement updatePatientByPatientID;
    private PreparedStatement selectPatientByPatientID;
    private PreparedStatement selectAllInfoByPatientID;
    
    // Constructor
    public HospitalDA() throws HospitalException {
        try
        {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            
            insertNewDoctor = connection.prepareStatement("INSERT INTO Doctor (doctorName, doctorType)" + "VALUES (?, ?)");
            updatePatientByPatientID = connection.prepareStatement("UPDATE Patient SET patientName = ? WHERE patientID = ?");
            selectPatientByPatientID = connection.prepareStatement("SELECT * FROM Patient WHERE patientID = ?");
            selectAllInfoByPatientID = connection.prepareStatement("SELECT * FROM DOCTOR INNER JOIN APPOINTMENT ON DOCTOR.doctorID = APPOINTMENT.doctorID INNER JOIN PATIENT ON APPOINTMENT.patientID = PATIENT.patientID WHERE APPOINTMENT.patientID = ?");
        }
        catch (SQLException sqlException){
            throw new HospitalException( sqlException.getMessage() );
        }
    }
    
    // Get specific patient information
    // id : specific patient ID
    public Patient getPatientByPatientID(int id) throws HospitalException {
        Patient wantedPatient = null;
        ResultSet dataFromDB = null;
        
        try {            
            // Set SQL Parameter
            selectPatientByPatientID.setInt(1, id);
            
            // Execute SQL and get data from database
            dataFromDB = selectPatientByPatientID.executeQuery();
            
            // Process data from database
            wantedPatient = new Patient();            
            while( dataFromDB != null && dataFromDB.next() ) {
                wantedPatient.setPatientID( dataFromDB.getInt("patientID") );
                wantedPatient.setDateOfBirth( dataFromDB.getDate("patientDateOfBirth") );
                wantedPatient.setName( dataFromDB.getString("patientName") );
            }
            
        } catch (SQLException sqlException){
            throw new HospitalException( sqlException.getMessage() );
        } finally {
            try {
                if( dataFromDB != null)
                    dataFromDB.close();
            } catch (SQLException sqlException){              
                close();
                throw new HospitalException( sqlException.getMessage() );
            }
        }
        
        return wantedPatient;
    }
    
    // Add new doctor information to database
    // INSERT INTO Doctor (doctorName, doctorType)" + "VALUES (?, ?) --> First ? mark = name, Second ? mark = type
    public int addDoctor(String name, String type) throws HospitalException {
        int returnRow = 0;
        
        try {
            // Parameter of SQL Statement
            insertNewDoctor.setString(1, name);
            insertNewDoctor.setString(2, type);
            
            returnRow = insertNewDoctor.executeUpdate();
        } catch (SQLException sqlException){
            close();
            throw new HospitalException( sqlException.getMessage() );
        }
        
        return returnRow;
    }
    
    // Get appointment information for specific patient
    // id : Specific patient ID
    public ArrayList<DisplayInfo> getAllInformationByPatientID(int id) throws HospitalException {
        ArrayList<DisplayInfo> list = new ArrayList<DisplayInfo>();
        DisplayInfo data = null;
        ResultSet dataFromDB = null;
        
        try {
            selectAllInfoByPatientID.setInt(1, id);
            
            // Execute SQL and get the return
            dataFromDB = selectAllInfoByPatientID.executeQuery();
            
            // Process data from database           
            while( dataFromDB.next() ) {      
                data = new DisplayInfo();
                data.setName("Appointment Date:");
                data.setData( dataFromDB.getDate("appointmentDate").toString() );
                list.add(data);
                
                data = new DisplayInfo();   
                data.setName("Appointment Time:");
                data.setData( dataFromDB.getTime("appointmentTime").toString() );             
                list.add(data);
                
                data = new DisplayInfo();
                data.setName("Doctor Name:");
                data.setData( dataFromDB.getString("doctorName") );
                list.add(data);
                
                data = new DisplayInfo();
                data.setName("Doctor Type:");
                data.setData( dataFromDB.getString("doctorType") );
                list.add(data);
                
                data = new DisplayInfo();
                data.setName("Patient Date of Birth:");
                data.setData( dataFromDB.getDate("patientDateOfBirth").toString() );
                list.add(data);
                
                data = new DisplayInfo();
                data.setName("Patient Name:");
                data.setData( dataFromDB.getString("patientName") );
                list.add(data);
            }
        } catch (SQLException sqlException){
            close();
            throw new HospitalException( sqlException.getMessage() );
        }
        
        return list;
    }
    
    public void updatePatientInformationByPatientID(int id, String name) throws HospitalException {
        try {
            updatePatientByPatientID.setString(1, name);
            updatePatientByPatientID.setInt(2, id);
            updatePatientByPatientID.executeUpdate();
        } catch (SQLException sqlException){
            close();
            throw new HospitalException( sqlException.getMessage() );
        }
    }
    
    public void close() throws HospitalException {
        try {
            connection.close();
        } catch (SQLException sqlException){
            throw new HospitalException( sqlException.getMessage() );
        }
    }
}
