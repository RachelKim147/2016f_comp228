/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise1;

/**
 *
 * @author Yeonju
 */
public class HospitalException extends Exception {
    
    // Constructor
    public HospitalException(String msg) {
        super(msg);
    }
    
}
