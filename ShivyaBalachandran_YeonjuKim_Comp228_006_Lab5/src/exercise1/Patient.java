/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise1;

import java.sql.Date;

/**
 *
 * @author Yeonju
 */
public class Patient {
    private int patientID;
    private Date dateOfBirth;
    private String name;
    
    // Constructor
    public Patient() {
        
    }
    
    // Constructor
    // strDateOfBirth format : 'yyyy-mm-yy'
    public Patient(int id, String strDateOfBirth, String name) {
        this.patientID = id;
        this.dateOfBirth = Date.valueOf(strDateOfBirth);
        this.name = name;
    }
    
    // Getter and Setter methods
    public void setPatientID(int id) {
        this.patientID = id;
    }
    
    public int getPatientID() {
        return this.patientID;
    }
    
    public void setDateOfBirth(String strDateOfBirth) {
        this.dateOfBirth = Date.valueOf(strDateOfBirth);
    }
    
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    
    public String getDateOfBirth() {
        return this.dateOfBirth.toString();
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    // End Getter and Setter methods
}
