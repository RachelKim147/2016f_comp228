/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise1;

/**
 *
 * @author Yeonju
 */
public class Doctor {
    private int doctorID;
    private String name;
    private String type;
    
    // Constructor
    public Doctor() {
        
    }
    
    // Constructor
    public Doctor(int id, String name, String type) {
        this.doctorID = id;
        this.name = name;
        this.type = type;
    }
    
    // Getter and Setter methods
    public void setDoctorID(int id) {
        this.doctorID = id;
    }
    
    public int getDoctorID() {
        return this.doctorID;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    public String getType() {
        return this.type;
    }
    // End Getter and Setter methods
} 
