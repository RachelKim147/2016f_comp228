/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise1;

/**
 *
 * @author Yeonju
 */
public class Appointment {
    private int appointmentID;
    private int doctorID;
    private int patientID;
    private String time;
    private String date;
    
    // Constructor
    public Appointment() {
    }
    
    // Constructor
    public Appointment(int appointmentID, int doctorID, int patientID, String time, String date) {
        this.appointmentID = appointmentID;
        this.doctorID = doctorID;
        this.patientID = patientID;
        this.time = time;
        this.date = date;
    }
    
    // Getter and Setter methods
    public int getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(int appointmentID) {
        this.appointmentID = appointmentID;
    }

    public int getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(int doctorID) {
        this.doctorID = doctorID;
    }

    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }
    // End getter and Setter methods

    public void setDate(String date) {
        this.date = date;
    }
    
    
    
}
