DROP TABLE Appointment;
DROP TABLE Doctor;
DROP TABLE Patient;


CREATE TABLE Doctor (
	doctorID INT NOT NULL GENERATED ALWAYS AS IDENTITY,
	doctorName varchar (100) NOT NULL,
	doctorType varchar (20) NOT NULL,
	PRIMARY KEY (doctorID)
);

CREATE TABLE Patient (
	patientID INT NOT NULL GENERATED ALWAYS AS IDENTITY,
	patientDateOfBirth date NOT NULL,
	patientName varchar (100) NOT NULL,
	PRIMARY KEY (patientID)
);

CREATE TABLE Appointment (
	appointmentID INT NOT NULL GENERATED ALWAYS AS IDENTITY,
	doctorID INT NOT NULL,
	patientID INT NOT NULL,
	appointmentTime time NOT NULL,
	appointmentDate date NOT NULL,	
	PRIMARY KEY (appointmentID),
	FOREIGN KEY (doctorID) REFERENCES Doctor (doctorID), 
	FOREIGN KEY (patientID) REFERENCES Patient (patientID)
);


INSERT INTO Doctor (doctorName, doctorType)
VALUES
	('Tim Brown', 'Audiologist'),
	('Tony Green', 'Allergist'),
	('Larisa Red', 'Dentist');
	
INSERT INTO Patient (patientDateOfBirth, patientName)
VALUES
	(DATE('06/30/2006'), 'Katar Aghavni'),
	(DATE('12/06/1990'), 'Berjouhi Mariam'),
	(DATE('02/10/2006'), 'Edvard Sarkis'),
	(DATE('08/21/2006'), 'Ararat Haik');
	
INSERT INTO Appointment (doctorID, patientID, appointmentTime, appointmentDate)
VALUES
	(1, 1, TIME('17:30:00'),DATE('06/06/2016')),
	(2, 2, TIME('10:00:00'),DATE('06/06/2016')),
	(3, 3, TIME('13:30:00'),DATE('11/25/2016')),
	(1, 4, TIME('09:30:00'),DATE('12/01/2016')),
	(2, 4, TIME('10:30:00'),DATE('12/01/2016'));
