package excercise1;

/**
 * 
 * @author Yeonju Kim (300899110)
 * Employee information class
 *
 */
public class Employee {
	private int employeeID;
	private String firstName;
	private String lastName;
	private double salary;
	
	/* Default Constructor */
	public Employee() {
	}
	
	/* Multi Argument Constructor */
	public Employee(int ID, String fName, String lName, double salary) {
		this.employeeID = ID;
		this.firstName = fName;
		this.lastName = lName;
		this.salary = salary;
	}
	
	/* Getter and setter methods */
	public int getEmployeeID() {
		return employeeID;
	}
	public void setEmployeeID(int employeeID) {
		this.employeeID = employeeID;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	/* Return all of employee information by string. */
	public String getEmployeeInformation()
	{
		String nicelyReturn;
		
		nicelyReturn = String.format( "The employee information:%nID: %d%nFirst name: %s%nLast name: %s%nSalary: %.2f%n",
				this.employeeID,
				this.firstName,
				this.lastName,
				this.salary);
		
		return nicelyReturn;
	}
	
	
}
