package excercise1;

import java.util.Scanner;

/**
 * 
 * @author Yeonju Kim (300899110)
 * Driver class for the employee
 *
 */
public class EmployeeDriver {

	public static void main(String[] args) {
		/* Input from console. */
		Scanner input = new Scanner(System.in);
		
		/* Create first employee. */
		Employee employeeFirst = new Employee(1, "Bill", "Smith", 50000.00f);
		
		
		/* Create second employee. */
		Employee employeeSecond = new Employee();
		
		/* Get second employee information from console. */
		System.out.print("Please enter employee ID: ");
		int theID = input.nextInt();
		employeeSecond.setEmployeeID(theID);
		
		System.out.print("Please enter first name: ");
		String theFirstName = input.next();
		employeeSecond.setFirstName(theFirstName);
			
		System.out.print("Please enter last name: ");
		String theLastName = input.next();
		employeeSecond.setLastName(theLastName);
		
		System.out.print("Please enter salary: ");
		double theSalary = input.nextDouble(); 
		/* If salary is negative number, try to get again. */
		while(theSalary < 0.f) {
			System.out.print("The value provided is negative. Please enter salary again: ");
			theSalary = input.nextDouble(); 
		}
		employeeSecond.setSalary(theSalary);
		
		/* Print employee information. */
		System.out.print("Please enter first name of the employee that you would like to see the detail about: ");
		String theSearchingName = input.next();
		if(theSearchingName.equalsIgnoreCase(employeeFirst.getFirstName())) 
			System.out.printf("%n%s",employeeFirst.getEmployeeInformation());
		else if(theSearchingName.equalsIgnoreCase(employeeSecond.getFirstName()))
			System.out.printf("%n%s",employeeSecond.getEmployeeInformation());
		else
			System.out.printf("%nSorry. There is no information about %s%n", theSearchingName);
		
		/* Close input. */
		input.close();		
	}

}
