package excercise2;

/**
 * 
 * @author Yeonju Kim (300899110)
 * Bank account information class
 *
 */
public class BankAccount {
	private int accountNumber;
	private String ownerName;
	private double balance;
	
	// Default Constructor
	public BankAccount() {
		this.accountNumber = 1001;
		this.ownerName = "Bill";
		this.balance = 100.00f;
	}
	
	// Getter methods
	public int getAccountNumber() {
		return accountNumber;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public double getBalance() {
		return balance;
	}
	
	// Return all of bank account information by string.
	public String getAccountInfo(boolean bInit) {
		String nicelyResult;
		String welcomeString;
		
		if(bInit){
			welcomeString = "Initial Bank Accont Information:";
		} else {
			welcomeString = "Current Bank Accont Information:";
		}
		
		nicelyResult = String.format( "%s%nAccount Number%5sName%5sBalance%n--------------------------------------------------%n%20s%d%8s%s%8s%.2f%n",
				welcomeString,
				" ", " ", " ", // for space
				this.accountNumber,
				" ", // for space
				this.ownerName,
				" ", // for space
				this.balance);
		
		return nicelyResult;
	}
	
	public void withdraw(double withdrawMount) {
		this.balance -= withdrawMount;
	}
	
	public void deposit(double depositMount) {
		this.balance += depositMount;
	}
}
