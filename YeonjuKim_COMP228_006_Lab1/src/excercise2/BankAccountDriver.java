package excercise2;

import javax.swing.JOptionPane;

/**
 * 
 * @author Yeonju Kim (300899110)
 * Driver class for the bank account
 *
 */
public class BankAccountDriver {

	public static void main(String[] args) {
		
		// Create Bill's account.
		BankAccount billAccount = new BankAccount();
		
		// Show account info and bank menu.
		Object menuMessage = String.format("%sPlease select option.", 
								billAccount.getAccountInfo(true));
		
		
		Object[] bankMenu = { "Withdraw", "Deposit" }; // Drop down menu
		Object selectedValue = JOptionPane.showInputDialog(null,
					menuMessage, "Welcome",
					JOptionPane.PLAIN_MESSAGE, null,
					bankMenu, bankMenu[0]);
		
		// Do withdraw or deposit
		if(selectedValue == bankMenu[0]) { // withdraw
			
			// Show withdraw dialog.
			String wantedMountString = JOptionPane.showInputDialog("How mech would you like to withdraw?");
			
			double wantedMount = Double.parseDouble(wantedMountString);	
			
			String underZeronErrorStr = "Withdrawal amount iz under zero.";
			String overErrorStr = "Withdrawal amount exceeded account balance.";
			String finalErrorStr = "";
			
			// Check withdraw is available or not.
			boolean bFail = false;
			double curBalance = billAccount.getBalance();
			if(wantedMount > curBalance) {
				
				// for fail message
				bFail = true;
				finalErrorStr = overErrorStr;
				
			} 
			else if(wantedMount < 0) {
				
				// for fail message
				bFail = true;
				finalErrorStr = underZeronErrorStr;
				
			}
			else {
				
				bFail = false;
				billAccount.withdraw(wantedMount);
				
			} 
			
			// Set message for dialog. If withdrawal is fail, add fail message.
			menuMessage = String.format("%s%n%s", 
											(bFail ? finalErrorStr : ""),
											billAccount.getAccountInfo(false));			
			
		} 
		else if(selectedValue == bankMenu[1]) { // deposit
			
			// Show deposit dialog.
			String wantedMountString = JOptionPane.showInputDialog("How mech would you like to deposit?");
			
			double wantedMount = Double.parseDouble(wantedMountString);	
			
			// Check deposit is available or not.
			boolean bFail = false;
			if(wantedMount < 0) {
				
				// for fail message
				bFail = true;
				
			}
			else {
				
				bFail = false;
				billAccount.deposit(wantedMount);
			
			}
			
			// Set message for dialog. If deposit is fail, add fail message.
			menuMessage = String.format("%s%n%s", 
											(bFail ? "Deposit amount is under zero." : ""),
											billAccount.getAccountInfo(false));	
		
		}
		else { // Do nothing.
			return; 
		}
		
		// Show result dialog.
		Object[] buttonOptions = {"OK"}; // Button
		JOptionPane.showOptionDialog(null,
										menuMessage, "Result", 		
										JOptionPane.PLAIN_MESSAGE, JOptionPane.PLAIN_MESSAGE, null,						
										buttonOptions, buttonOptions[0]); 				
	}

}
