package exercise1;

import java.security.SecureRandom;

import javax.swing.JOptionPane;

public class Test {

	// How many questions do you have?
	private final static int MAXCOUNTOFQUESTION = 5;
	// How many question do you answer correctly?
	private int countCorrectAnswers;
	// Now what question do you try to solve?
	private int currentQuestion = 0;
	
	// Questions
	private final String questionMessage1 = "Q1. Which print method outputs a new line every time?";
	private final Object[] answersOfQuestion1 = {"print()", "println()", "printf()", "none of above"};
	
	private final String questionMessage2 = "Q2. What is the result of 123.457 using '%%.2f' format specifier?";
	private final Object[] answersOfQuestion2 = {"123.457", "123.45", "123.46", "12.457"};
	
	private final String questionMessage3 = "Q3. What is the result of following code?\nSecureRandom randomNumbers = new SecureRandom();\nint ramdumResult = randomNumbers.nextInt(4);";
	private final Object[] answersOfQuestion3 = { "ramdumResult will be contain an integer number between 0~3.", 
													"ramdumResult will be contain an integer number between 1~4.", 
													"ramdumResult will be contain an integer number more than 4.", 
													"ramdumResult will be contain an integer number 4."};
	
	private final String questionMessage4 = "Q4. Which one is correct when calling the flowing method?\n public static void addTwoValue(int first, int second) {\n int result = first + second;\n }";
	private final Object[] answersOfQuestion4 = {"int result = addTwoValue(1, 2);", 
													"double result = addTwoValue(2.1, 3.5);",
													"addTwoValue(int a, int b);", 
													"none of above."};
	
	private final String questionMessage5 = "Q5. Which one is correct default value for type?";
	private final Object[] answersOfQuestion5 = {"int - 0", "double - 0.0d", "String - null", "all of above"};

	// Default constructor
	public Test() {
		this.countCorrectAnswers = 0;
		this.currentQuestion = 0;
	}

	//
	public void startTheTest() {

		// Display welcome message and check the user want to take test or not.
		int result = JOptionPane.showConfirmDialog(null, "Are you ready to test?", "Welcome",
				JOptionPane.YES_NO_OPTION);

		if (result == JOptionPane.YES_OPTION) {
			simulateQuestion();
			displaySummary();
		}
	}

	private void simulateQuestion() {
		
		for(currentQuestion = 0; currentQuestion < MAXCOUNTOFQUESTION; currentQuestion++) {
			switch (currentQuestion) {
			case 0:
				doTest1();				
				break;
			case 1:
				doTest2();
				break;
			case 2:
				doTest3();
				break;
			case 3:
				doTest4();
				break;
			case 4:
				doTest5();
				break;
			}			
		}
	}

	private void checkAnswer(int questionNumber, Object answeredValue) {
		boolean result;
		String message;

		switch (questionNumber) {
		case 0:
			result = (answeredValue == answersOfQuestion1[1]) ? true : false;
			break;
		case 1:
			result = (answeredValue == answersOfQuestion2[2]) ? true : false;
			break;
		case 2:
			result = (answeredValue == answersOfQuestion3[0]) ? true : false;
			break;
		case 3:
			result = (answeredValue == answersOfQuestion4[3]) ? true : false;
			break;
		case 4:
			result = (answeredValue == answersOfQuestion5[3]) ? true : false;
			break;
		default:
			result = false;
		}
		
		// Create the message depends on the answer.
		message = generateMessage(result);
		// If the answer is correct add answer count.
		if (result)
			countCorrectAnswers++;

		// Then, show result dialog.
		JOptionPane.showMessageDialog(null, message);
	}
	
	private String generateMessage(boolean bCorrect) {
		SecureRandom randomNumbers = new SecureRandom();
		int ramdumResult = 1 + randomNumbers.nextInt(4);

		switch (ramdumResult) // 4 : max boundary
		{
		case 1:
			return bCorrect ? "Excellent!" : "No. Please try again";
		case 2:
			return bCorrect ? "Very good!" : "Wrong. Try once more";
		case 3:
			return bCorrect ? "Keep up the good work!" : "Don't give up";
		case 4:
			return bCorrect ? "Nice work!" : "No. Keep trying..";
		default:
			return "Error!";
		}
	}

	private void displaySummary() {
		double resultPercentage = ((double) countCorrectAnswers / (double) MAXCOUNTOFQUESTION) * 100.f;
		String resultMessage = String.format("Here is your result.%n%.2f%%%n%d/%d (Correct Answers/Total Answers)%n",
				resultPercentage, countCorrectAnswers, MAXCOUNTOFQUESTION);
		JOptionPane.showMessageDialog(null, resultMessage);
	}
	
	// Display drop-down dialog for questions.
	private Object createQuestionDialog(String questionMessage, Object[] answerList) {
		
		Object selectedValue = JOptionPane.showInputDialog(null,
				questionMessage, "Java test",
				JOptionPane.QUESTION_MESSAGE, null,
				answerList, answerList[0]);	
		
		return selectedValue;
	}
	
	private void doTest1() {
		// Create dialog and get user's answer.
		Object answeredValue = createQuestionDialog( questionMessage1, answersOfQuestion1 );

		// Check the answer is correct and make result message.
		checkAnswer( currentQuestion, answeredValue );
	}

	private void doTest2() {
		// Create dialog and get user's answer.
		Object answeredValue = createQuestionDialog( questionMessage2, answersOfQuestion2 );

		// Check the answer is correct and make result message.
		checkAnswer( currentQuestion, answeredValue );
	}

	private void doTest3() {
		// Create dialog and get user's answer.
		Object answeredValue = createQuestionDialog( questionMessage3, answersOfQuestion3 );

		// Check the answer is correct and make result message.
		checkAnswer( currentQuestion, answeredValue );
	}

	private void doTest4() {
		// Create dialog and get user's answer.
		Object answeredValue = createQuestionDialog( questionMessage4, answersOfQuestion4 );

		// Check the answer is correct and make result message.
		checkAnswer( currentQuestion, answeredValue );
	}

	private void doTest5() {
		// Create dialog and get user's answer.
		Object answeredValue = createQuestionDialog( questionMessage5, answersOfQuestion5 );

		// Check the answer is correct and make result message.
		checkAnswer( currentQuestion, answeredValue );
	}
}
