package exercise3;

public class OverloadedStaticMethods {
	public static void main(String[] args) {
		
		// int
		System.out.printf("Multiply integer 7 and integer 8 : %d%n", multiply(7, 8));
		
		// double
		System.out.printf("Multiply double 1.5 and double 2.6 : %f%n", multiply(1.5, 2.6));
		
		// float
		System.out.printf("Multiply float 1.4f and float 1.5f : %f%n", multiply(1.4f, 1.5f));
	
	}
	
	// overloaded function for int
	private static int multiply(int firstValue, int secondValue) {
		System.out.printf( "Called multiply method with int argument: %d, %d%n", firstValue, secondValue );
		return firstValue * secondValue;
	}
	
	// overloaded function for double
	private static double multiply(double firstValue, double secondValue) {
		System.out.printf( "Called multiply method with double argument: %f, %f%n", firstValue, secondValue );
		return firstValue * secondValue;
	}
	
	// overloaded function for float
	private static float multiply(float firstValue, float secondValue) {
		System.out.printf( "Called multiply method with float argument: %f, %f%n", firstValue, secondValue );
		return firstValue * secondValue;
	}
}
