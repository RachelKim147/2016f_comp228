package exercise2;

import java.security.SecureRandom;
import java.util.ArrayList;

public class Lotto {
	private ArrayList<Integer> lottoNumbers;
	private final static int MAX_COUNT_OF_LOTTO_NUMBERS = 3;
	
	// default constructor
	// a constructor that randomly populates the array for a lotto object
	public Lotto() {
		
		lottoNumbers = new ArrayList<Integer>();
		SecureRandom randomNumbers = new SecureRandom();
		int loopCount = 0;
		int ramdomResult = 0; // reuse
		
		while( loopCount < MAX_COUNT_OF_LOTTO_NUMBERS ) {
			// + 1 : (0~8) -> (1~9)
			ramdomResult = 1 + randomNumbers.nextInt(9); 			
			lottoNumbers.add(ramdomResult);
			loopCount++;
		}	
	}
	
	// getter
	// include a method in the class to return the array
	public ArrayList<Integer> getLottoNumbers() {
		
		return lottoNumbers;
		
	}
}
