package exercise2;

import java.util.ArrayList;

import javax.swing.JOptionPane;

public class LottoDriver {
	
	// Const variables
	private static final int MAX_GAME_COUNT = 5; // we will play lotto games for 5 times.
	private static final String welcomeMessage = "Please enter number(3~27).";
	private static final String welcomeMessageWithError = "Invaild number!\nPlease enter number(3~27).";
	
	public static void main(String[] args) {
		
		int userChosenNumber;
		int computerChosenNumber;
		
		for( int i = 0; i < MAX_GAME_COUNT; i++ ) {
			
			userChosenNumber = inputUserChosenSumNumber( i );
			computerChosenNumber = createComputerChosenSumNumber();
			
			if( userChosenNumber == computerChosenNumber ) {
				JOptionPane.showMessageDialog( null, String.format("Lotto Round %d%nUser Win!", i+1) );
				return;
			}
			else {
				JOptionPane.showMessageDialog( null, String.format("Lotto Round %d%nNumber you selected: %d%nNumber computer selected: %d", i+1, userChosenNumber, computerChosenNumber) );
			}
			
		}
		
		// Computer Win
		JOptionPane.showMessageDialog( null, "Computer Win!" );
	}
	
	// Get number(3~27) from dialog.
	public static int inputUserChosenSumNumber(int gameCount) {
		
		String answeredStr; // reuse
		int answeredNumner;
		boolean bSuccess = true;
		
		while(true) {
			// display dialog and get value from input UNTIL the user types correct number.
			answeredStr = JOptionPane.showInputDialog( String.format("Lotto Round %d%n", (gameCount+1)) + (bSuccess? welcomeMessage : welcomeMessageWithError) );
			
			// if user click "cancel" button or no input, it is fail.
			try{
				answeredNumner = Integer.parseInt( answeredStr );
				  
			} catch (NumberFormatException e) {
				  
				answeredNumner = -1;
			}
						
			// check the value is valid or not.
			bSuccess = verifyInputValue( answeredNumner );
			// if valid, escape while statement.
			if( bSuccess ) {
				return answeredNumner;
			} 		
		}
	}
	
	// Check the number what user types.
	public static boolean verifyInputValue(int value) {
		
		if( value < 3 || value > 27 )
			return false;
		
		return true;
	}
	
	public static int createComputerChosenSumNumber() {
		
		Lotto comLotto = new Lotto(); 
		
		// Get three numbers.
		ArrayList<Integer> chosenNumberList = comLotto.getLottoNumbers();
		
		// Sum all them.
		int sum = 0;
		for( int number : chosenNumberList ) {
			sum += number;
		}
		
		return sum;
	}

}
