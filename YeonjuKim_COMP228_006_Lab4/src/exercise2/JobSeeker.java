/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise2;

import java.util.ArrayList;

/**
 *
 * @author Yeonju Kim
 */
public class JobSeeker {
    private String firstName;
	private String lastName;
	private String gender;
	private String phoneNumber;
	private double salary;
	private String interest;
	private ArrayList<String> typeOfEmployments;
	private ArrayList<String> locations;
	
	public JobSeeker() {
            typeOfEmployments = new ArrayList<String>();
            locations = new ArrayList<String>();
	}

	public void setFirstName(String firstName) {
            this.firstName = firstName;
	}

	public void setLastName(String lastName) {
            this.lastName = lastName;
	}

	public void setGender(String gender) {
            this.gender = gender;
	}

	public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
	}

	public void setSalary(double salary) {
            this.salary = salary;
	}

	public void setInterest(String interest) {
            this.interest = interest;
	}

	public void setTypeOfEmployments(ArrayList<String> typeOfEmployments) {
            this.typeOfEmployments = typeOfEmployments;
	}

	public void setLocations(ArrayList<String> locations) {
            this.locations = locations;
	}
	
	protected String getAllTypeOfEmploymentString() {
            String result = "";
		
            for (String emp : typeOfEmployments) {
		result += String.format("%s ", emp);
            }
		
            return result;
	}
	
	protected String getAllLocationsString() {
            String result = "";
		
            for (String loc : locations) {
		result += String.format("%s ", loc);
            }
		
            return result;
	}
	
	public String getDisplayInfo() {
            String result;
		
            result = String.format("First name: %s%nLast name: %s%n", firstName, lastName);
            result += String.format("Gender: %s%n", gender);
            result += String.format("Phone Number: %s%n", phoneNumber);
            result += String.format("Salary: $%.2f%n", salary);
            result += String.format("Interest: %s%n", interest);
            result += String.format("Type of Employment: %s%n", getAllTypeOfEmploymentString());
            result += String.format("Locations: %s", getAllLocationsString());
		
            return result;
	}
}
