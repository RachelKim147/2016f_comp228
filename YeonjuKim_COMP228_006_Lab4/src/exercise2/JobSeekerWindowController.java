package exercise2;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;

/**
 *
 * @author Yeonju Kim
 */
public class JobSeekerWindowController implements Initializable {
    
    @FXML
    private TextField tfName;
    
    @FXML
    private TextField tfSurname;
    
    @FXML
    private TextField tfPhone;
    
    @FXML
    private TextArea taDisplay;
    
    @FXML
    private ComboBox cbFieldOfInterest;
    
    @FXML
    private ListView lvPreferLoc;
    
    @FXML
    private Button btDisplay;
    
    @FXML
    private RadioButton rbFemale;
    
    @FXML
    private RadioButton rbMale;
    
    @FXML
    private ToggleGroup gender;
    
    @FXML
    private CheckBox cbFullTime;
    
    @FXML
    private CheckBox cbPartTime;
    
    @FXML
    private CheckBox cbVolunteer;
    
    @FXML
    private CheckBox cbSummer;
    
    @FXML
    private TextField tfExpectedSalary;
    
    ObservableList<String> interests = FXCollections.observableArrayList(
        "Computer and Information Research Scientist", "Computer Network Architect", "Computer-Control Programmers and Operators", 
        "Computer Programmers", "Computer and Information Systems Managers", "Computer Science",
	"Computer, Automated Teller, and Office Machine Repairers", "Computer Hardware Engineer",
	"Computer Operators", "Computer Software Engineer",
	"Computer Support Specialists and Systems Administrators", "Computer Systems Analyst",
	"Database Administrator", "Information Security Analyst", "Mathematician (Math)", "Statistician"  );
    
    ObservableList<String> locations = FXCollections.observableArrayList(
        "Toronto", "Ottawa", "Edmonton", "Quebec City", "Calgary", 
	"Montreal", "Hamilton", "Kingston", "Vancouver", "Winnipeg", "London", "Greater Sudbury", "Mississauga", "Brampton");
    
    @FXML
    private void handleButtonAction(ActionEvent event) throws WrongInputException {
        JobSeeker newInfo = new JobSeeker();
        
        newInfo.setFirstName( tfName.getText() );
		
	newInfo.setLastName( tfSurname.getText() );
		
	newInfo.setPhoneNumber( tfPhone.getText() );

        newInfo.setGender(getSelectedGender());
        
        newInfo.setInterest(cbFieldOfInterest.getValue().toString());
        
        ObservableList<String> selectedItems =  lvPreferLoc.getSelectionModel().getSelectedItems();
        newInfo.setLocations( new ArrayList<>(selectedItems) );
       
        newInfo.setTypeOfEmployments( getSelectedTypeOfEmployment() );
        
        double inputSalary = 0.0;
	try {
            inputSalary = Double.parseDouble(tfExpectedSalary.getText());
            if( inputSalary < 0.0 ) 
            {
                inputSalary = 0.0;
                throw new WrongInputException("Negative number");
            }
	} catch( Exception e ) {
            Alert alert = new Alert(AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Expected Salary");
            String error = e.toString();
            alert.setContentText( error );
            alert.showAndWait();
	} finally {
            newInfo.setSalary( inputSalary );
	}
        
        taDisplay.setText(newInfo.getDisplayInfo());
    }
    
    private String getSelectedGender() {
        Toggle selectedToggle = gender.getSelectedToggle();
        if( selectedToggle != null ) {
            Object selectedUserdata = selectedToggle.getUserData();
            if( selectedUserdata != null )
                return selectedUserdata.toString();
        }
        
        return "";
    }
    
    private ArrayList<String> getSelectedTypeOfEmployment() {
        ArrayList<String> list = new ArrayList<String>();
        
        if( cbFullTime.isSelected() )
            list.add( cbFullTime.getText() );
        
        if( cbPartTime.isSelected() )
            list.add( cbPartTime.getText() );
        
        if( cbVolunteer.isSelected() )
            list.add( cbVolunteer.getText() );
        
        if( cbSummer.isSelected() )
            list.add( cbSummer.getText() );
            
        return list;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        cbFieldOfInterest.setItems(interests);    
        cbFieldOfInterest.setValue(interests.get(0));
        
        lvPreferLoc.setItems(locations);
        lvPreferLoc.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        
        rbFemale.setUserData("Female");
        rbMale.setUserData("Male");
    }    
    
    private class WrongInputException extends Exception {
	public WrongInputException(String error) {
            super(error);
	}
    }
}
