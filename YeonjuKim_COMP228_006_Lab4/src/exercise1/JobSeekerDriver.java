package exercise1;

import javax.swing.JFrame;

public class JobSeekerDriver {

	public static void main(String[] args) {
		JobSeekerFrame jobSeekerFrame = new JobSeekerFrame();
		jobSeekerFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jobSeekerFrame.setSize(1550, 400);
		jobSeekerFrame.setVisible(true);
	}

}
