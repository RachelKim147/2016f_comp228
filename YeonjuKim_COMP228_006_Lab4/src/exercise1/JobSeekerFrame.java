package exercise1;

import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;

public class JobSeekerFrame extends JFrame implements ActionListener{
	// Main panels
	private JPanel[] groupPanels;
	
	// Group Panel 1
	private JPanel namePanel;
		private JLabel 				firstNameLabel;
		private JTextField 			firstNameTextField;
		private JLabel 				surNameLabel;
	private JTextField surNameTextField;
		private JPanel 				phonePanel;
		private JLabel 				phoneLabel;
		private JTextField 			phoneTextField;
	private JPanel genderPanel;
		private JLabel 				genderLabel;
		private ButtonGroup 		genderGroup;
		private JRadioButton[] 		genderRadioButtons;
		private final String[] 		genderRadioButtonLabels = { "Female", "Male" };
	// Group Panel 2
	private JPanel jobFieldPanel;
		private JLabel 				jobFieldLabel;
		private JComboBox<String>	jobFieldList;
		private final String[] 		fieldListLabels = { "Computer and Information Research Scientist",
				"Computer Network Architect", "Computer-Control Programmers and Operators", "Computer Programmers",
				"Computer and Information Systems Managers", "Computer Science",
				"Computer, Automated Teller, and Office Machine Repairers", "Computer Hardware Engineer",
				"Computer Operators", "Computer Software Engineer",
				"Computer Support Specialists and Systems Administrators", "Computer Systems Analyst",
				"Database Administrator", "Information Security Analyst", "Mathematician (Math)", "Statistician" };
	private JPanel locationPanel;
		private JLabel 				locationLabel;
		private JList<String> 		locationList;
		private final String[] 		locationListLabels = { "Toronto", "Ottawa", "Edmonton", "Quebec City", "Calgary", 
				"Montreal", "Hamilton", "Kingston", "Vancouver", "Winnipeg", "London", "Greater Sudbury", "Mississauga", "Brampton" };
	// Group Panel 3
	private JPanel typeOfEmploymentPanel;
		private JLabel 				typeOfEmploymentLabel;
		private JCheckBox[] 		typeOfEmploymentCheckBoxes;
		private final String[] 		typeOfEmploymentCheckBoxLabels = { "Full-time", "Part-time", "Volunteer", "Summer" };
	private JPanel expectedSalaryPanel;
		private JLabel				expectedSalaryLabel;
		private JTextField			expectedSalaryTextField;
	// Group Panel 4
	private JButton	displayButton;
	private JTextArea displayTextArea;
				
	public JobSeekerFrame() {
		super("Job seeker");
		setLayout(new GridLayout(1, 4));
		
		// UI Layout
		//|------------------------------------
		//|        |		|		 |		  |
		//| Group1 | Group2	| Group3 | Group4 |
		//|		   |		| 		 |		  |
		//|-----------------------------------|
		groupPanels = new JPanel[4];
		for (int i = 0; i < 4; i++) {
			groupPanels[i] = new JPanel();
		}
		
		// Group1 : First name, Last name, Phone, and Gender
		createGroup1();
				
		// Group2 : Field of interest, Preferred locations
		createGroup2();
		
		// Group3 : Type of employment, Expected salary
		createGroup3();

		// Group4 : Display button, result 
		createGroup4();		
	}
	
	// Group1 : First name, Last name, Phone, and Gender
	private void createGroup1() {
		groupPanels[0].setLayout( new GridLayout(3, 1) );
		{
			// First name, Last name
			namePanel = new JPanel();
			namePanel.setLayout(new GridLayout(4, 1));
			namePanel.setBorder(new TitledBorder("Basic information"));
			{
				firstNameLabel = new JLabel("First name:");
				firstNameTextField = new JTextField(10);
				surNameLabel = new JLabel("Last name:");
				surNameTextField = new JTextField(10);

				namePanel.add(firstNameLabel);
				namePanel.add(firstNameTextField);
				namePanel.add(surNameLabel);
				namePanel.add(surNameTextField);
			}
			groupPanels[0].add(namePanel);
			// First name, Last name End
			
			// Phone
			phonePanel = new JPanel();
			phonePanel.setLayout(new GridLayout(2, 1));
			phonePanel.setBorder(new TitledBorder("Contact information"));
			{
				phoneLabel = new JLabel("Phone:");
				phoneTextField = new JTextField(10);

				phonePanel.add(phoneLabel);
				phonePanel.add(phoneTextField);
			}
			groupPanels[0].add(phonePanel);
			// Phone End
			
			// Gender
			genderPanel = new JPanel();
			genderPanel.setLayout(new GridLayout(3, 1));
			genderPanel.setBorder(new TitledBorder("Personal information"));
			{
				genderLabel = new JLabel("Gender:");
				genderPanel.add(genderLabel);

				genderGroup = new ButtonGroup();

				genderRadioButtons = new JRadioButton[genderRadioButtonLabels.length];
				for (int i = 0; i < genderRadioButtons.length; i++) {
					genderRadioButtons[i] = new JRadioButton(genderRadioButtonLabels[i]);
					genderGroup.add(genderRadioButtons[i]);
					genderPanel.add(genderRadioButtons[i]);
				}
			}
			groupPanels[0].add(genderPanel);
			// Gender End
		}	
		add(groupPanels[0]);
	}
	
	// Group2 : Field of interest, Preferred locations
	private void createGroup2() {
		groupPanels[1].setLayout( new GridLayout(2, 1) );
		{
			// Field of interest
			jobFieldPanel = new JPanel();
			jobFieldPanel.setLayout(new GridLayout(2, 1));
			jobFieldPanel.setBorder(new TitledBorder("Field information"));
			{
				jobFieldLabel = new JLabel("Field of interest:");
				jobFieldList = new JComboBox<String>(fieldListLabels);
				jobFieldList.setMaximumRowCount(5);

				jobFieldPanel.add(jobFieldLabel);
				jobFieldPanel.add(new JScrollPane(jobFieldList));
			}
			groupPanels[1].add(jobFieldPanel);
			// Field of interest End
			
			// Preferred locations
			locationPanel = new JPanel();
			locationPanel.setLayout(new GridLayout(2, 1));
			locationPanel.setBorder(new TitledBorder("Workplace information"));
			{
				locationLabel = new JLabel("Preferred location:");
				locationList = new JList<String>(locationListLabels);
				locationList.setVisibleRowCount(5);
				locationList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

				locationPanel.add(locationLabel);
				locationPanel.add(new JScrollPane(locationList));
			}
			groupPanels[1].add(locationPanel);
			// Preferred locations End
		}
		add(groupPanels[1]);
	}
	
	// Group3 : Type of employment, Expected salary
	private void createGroup3() {
		groupPanels[2].setLayout( new GridLayout(2, 1) );
		{
			typeOfEmploymentPanel = new JPanel();
			typeOfEmploymentPanel.setLayout(new GridLayout(5, 1));
			typeOfEmploymentPanel.setBorder(new TitledBorder("Detailed job information"));
			{
				typeOfEmploymentLabel = new JLabel("Type of employment:");
				typeOfEmploymentPanel.add(typeOfEmploymentLabel);
				
				typeOfEmploymentCheckBoxes = new JCheckBox[typeOfEmploymentCheckBoxLabels.length];
				for( int i = 0; i < typeOfEmploymentCheckBoxLabels.length; i++ ) {
					typeOfEmploymentCheckBoxes[i] = new JCheckBox(typeOfEmploymentCheckBoxLabels[i]);
					typeOfEmploymentPanel.add(typeOfEmploymentCheckBoxes[i]);
				}
			}
			groupPanels[2].add(typeOfEmploymentPanel);
			
			expectedSalaryPanel = new JPanel();
			expectedSalaryPanel.setLayout(new GridLayout(2, 1));
			expectedSalaryPanel.setBorder(new TitledBorder("Salary information"));
			{
				expectedSalaryLabel = new JLabel("Expected salary:");
				expectedSalaryTextField = new JTextField(10);
				
				expectedSalaryPanel.add(expectedSalaryLabel);
				expectedSalaryPanel.add(expectedSalaryTextField);
			}
			groupPanels[2].add(expectedSalaryPanel);
		}
		add(groupPanels[2]);
	}
	
	// Group4 : Display button, result 
	private void createGroup4() {
		groupPanels[3].setLayout( new FlowLayout() );
		{
			displayButton = new JButton("Display");
			displayButton.addActionListener(this);
			Box box = Box.createHorizontalBox();
			displayTextArea = new JTextArea("Result", 18, 30);
			box.add(new JScrollPane(displayTextArea));
			
			groupPanels[3].add(displayButton);
			groupPanels[3].add(box);
		}
		add(groupPanels[3]);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		JobSeeker newInfo = new JobSeeker();
		
		newInfo.setFirstName( firstNameTextField.getText() );
		
		newInfo.setLastName( surNameTextField.getText() );
		
		newInfo.setPhoneNumber( phoneTextField.getText() );
		
		for(int i = 0; i < genderRadioButtonLabels.length; i++) {
			if( genderRadioButtons[i].isSelected() ) {
				newInfo.setGender( genderRadioButtons[i].getText() );
				break;
			}
		}
		
		newInfo.setInterest( fieldListLabels[jobFieldList.getSelectedIndex()] );
		
		ArrayList<String> selectedLocations = new ArrayList<String>(locationList.getSelectedValuesList());
		newInfo.setLocations( selectedLocations );
		
		ArrayList<String> selectedTypeOfEmployments = new ArrayList<String>();
		for(int i = 0; i < typeOfEmploymentCheckBoxes.length; i++) {
			if( typeOfEmploymentCheckBoxes[i].isSelected() ) {
				selectedTypeOfEmployments.add(typeOfEmploymentCheckBoxes[i].getText());
			}
		}
		newInfo.setTypeOfEmployments(selectedTypeOfEmployments);
		
		double inputSalary = 0.0;
		try {
			inputSalary = Double.parseDouble(expectedSalaryTextField.getText());
			if( inputSalary < 0.0 )
				throw new WrongInputException("Negative number");
		} catch( Exception e ) {
			JOptionPane.showMessageDialog(null, 
					  						e.getMessage(),
					  						"Expected Salary", 
					  						JOptionPane.ERROR_MESSAGE);
		} finally {
			newInfo.setSalary( inputSalary );
		}
		
		
		displayTextArea.setText(newInfo.getDisplayInfo());
		
	}
	
	private class WrongInputException extends Exception {
		public WrongInputException(String error) {
			super(error);
		}
	}
}
